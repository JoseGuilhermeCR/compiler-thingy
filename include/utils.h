#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>
#include <string.h>

#define ARRLEN(x) (sizeof(x) / sizeof(x[0]))

#define ZEROPTR(x) memset((x), 0, sizeof(*(x)))

#define ASSERT_IMPL(expr)                             \
    do {                                              \
        if (!(expr)) {                                \
            assert_failed(__FILE__, __LINE__, #expr); \
        }                                             \
    } while (0)

#if !defined(NDEBUG)
#define ASSERT(expr) ASSERT_IMPL(expr)
#else
#define ASSERT(expr)
#endif

#define RASSERT(expr) ASSERT_IMPL(expr)

#define UNREACHABLE()                            \
    do {                                         \
        unreachable_reached(__FILE__, __LINE__); \
    } while (0)

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

[[noreturn]] void assert_failed(const char *file, i32 line, const char *expr);

[[noreturn]] void unreachable_reached(const char *file, i32 line);

#endif
