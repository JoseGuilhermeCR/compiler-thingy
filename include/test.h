#ifndef TEST_H_
#define TEST_H_

#include "utils.h"

typedef i32 (*test_fn_t)(void);

struct test_case {
    test_fn_t test;
    const char *name;
};

#define TEST(fn)                \
    {                           \
        .test = fn, .name = #fn \
    }

#endif
