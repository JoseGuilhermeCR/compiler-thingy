#ifndef TRACE_H_
#define TRACE_H_

#include "utils.h"

#define TRACE_INFO(fmt, ...) trace_info(__func__, __LINE__, fmt, ##__VA_ARGS__)

#define LOG_INFO(fmt, ...) log_info(fmt, ##__VA_ARGS__)

#define LOG_PERROR(fmt) log_perror(fmt)

void trace_info(const char *function, i32 line, const char *fmt, ...);

void log_info(const char *fmt, ...);

void log_perror(const char *fmt);

#endif
