#ifndef FILE_BUFFER_H_
#define FILE_BUFFER_H_

#include <stdint.h>

#include "utils.h"

struct file_buffer {
    const char *data;
    u64 size;
};

[[nodiscard]] i32 file_buffer_read(const char *pathname, struct file_buffer *file);

void file_buffer_destroy(struct file_buffer *file);

#endif
