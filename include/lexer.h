#ifndef LEXER_H_
#define LEXER_H_

#include <stdint.h>
#include <stdio.h>

#include "utils.h"

#define LEXER_MAX_LEXEME_SIZE 31

enum token {
    TOKEN_UNKNOWN,

    TOKEN_IDENTIFIER,

    // var
    TOKEN_VAR,
    // const
    TOKEN_CONST,
    // function
    TOKEN_FUNCTION,
    // return
    TOKEN_RETURN,
    // for
    TOKEN_FOR,
    // while
    TOKEN_WHILE,
    // break
    TOKEN_BREAK,
    // continue
    TOKEN_CONTINUE,
    // if
    TOKEN_IF,
    // else
    TOKEN_ELSE,

    // i8
    TOKEN_I8,
    // i16
    TOKEN_I16,
    // i32
    TOKEN_I32,
    // i64
    TOKEN_I64,
    // u8
    TOKEN_U8,
    // u16
    TOKEN_U16,
    // u32
    TOKEN_U32,
    // u64
    TOKEN_U64,
    // f32
    TOKEN_F32,
    // f64
    TOKEN_F64,
    // bool
    TOKEN_BOOL,
    // char
    TOKEN_CHAR,
    // string
    TOKEN_STRING,

    // True
    TOKEN_CONST_TRUE,

    // False
    TOKEN_CONST_FALSE,

    // 'a', '\n', ...
    TOKEN_CONST_CHAR,
    // "abc", "\n", ...
    TOKEN_CONST_STRING,
    // 0123456789
    TOKEN_CONST_INTEGER,
    //  1.23
    TOKEN_CONST_FP,

    // :
    TOKEN_COLON,
    // ;
    TOKEN_SEMICOLON,
    // ,
    TOKEN_COMMA,

    // =
    TOKEN_EQUAL,

    // ==
    TOKEN_EQUAL_EQUAL,
    // !=
    TOKEN_NOT_EQUAL,
    // >
    TOKEN_GREATER,
    // >=
    TOKEN_GREATER_EQUAL,
    // <
    TOKEN_LESS,
    // <=
    TOKEN_LESS_EQUAL,

    // +
    TOKEN_PLUS,
    // -
    TOKEN_MINUS,
    // /
    TOKEN_FORWARD_SLASH,
    // *
    TOKEN_ASTERISK,
    // %
    TOKEN_PERCENT,
    // !
    TOKEN_NOT,
    // &
    TOKEN_AMPERSAND,
    // |
    TOKEN_PIPE,
    // ^
    TOKEN_CIRCUMFLEX,
    // >>
    TOKEN_GREATER_GREATER,
    // <<
    TOKEN_LESS_LESS,

    // (
    TOKEN_LPAREN,
    // )
    TOKEN_RPAREN,

    // [
    TOKEN_LBRACKET,
    // ]
    TOKEN_RBRACKET,

    // {
    TOKEN_LCURLY,
    // }
    TOKEN_RCURLY,
};

struct lexeme {
    char data[LEXER_MAX_LEXEME_SIZE + 1];
    u32 size;
};

struct lexical_entry {
    struct lexeme lexeme;
    enum token token;
    u32 line;
};

enum lexer_result {
    LEXER_RESULT_ERROR,
    LEXER_RESULT_SUCCESS,
    LEXER_RESULT_EOF,
};

struct lexer {
    const char *string;
    u32 cursor;
    u32 line;
    char last_c;
};

[[nodiscard]] i32 lexer_init(const char *string, struct lexer *lexer);

[[nodiscard]] enum lexer_result
lexer_get_next_entry(struct lexer *lexer, struct lexical_entry *entry);

void lexer_print_entry(FILE *file, struct lexical_entry *entry);

#if defined(ENABLE_TESTS)

#include "test.h"

[[nodiscard]] const struct test_case *get_lexer_tests(void);
[[nodiscard]] i32 get_lexer_tests_len(void);

#endif

#endif
