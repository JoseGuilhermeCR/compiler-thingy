#include "trace.h"

#include <stdarg.h>
#include <stdio.h>

#include <errno.h>
#include <string.h>

static void impl_trace_info(const char *function, i32 line, const char *fmt,
                            va_list *list)
{
    fprintf(stderr, "[INFO] %s:%i => ", function, line);
    vfprintf(stderr, fmt, *list);
}

void trace_info(const char *function, i32 line, const char *fmt, ...)
{
    va_list fmt_list;
    va_start(fmt_list, list);
    impl_trace_info(function, line, fmt, &fmt_list);
    va_end(fmt_list);
}

static void impl_log_info(const char *fmt, va_list *list)
{
    fputs("[INFO] => ", stderr);
    vfprintf(stderr, fmt, *list);
}

void log_info(const char *fmt, ...)
{
    va_list fmt_list;
    va_start(fmt_list, list);
    impl_log_info(fmt, &fmt_list);
    va_end(fmt_list);
}

void log_perror(const char *fmt)
{
    const i32 err = errno;

    char buf[129];
    RASSERT(strerror_r(err, buf, sizeof(buf)) == 0);

    fprintf(stderr, "[ERROR] => %s: %s (%i)\n", fmt, buf, err);
}
