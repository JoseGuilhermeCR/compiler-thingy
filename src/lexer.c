#include "lexer.h"

#include <assert.h>
#include <ctype.h>
#include <stddef.h>

#include "trace.h"
#include <stdio.h>

#define COMMENT_STARTER '#'

enum lexer_state {
    LEXER_STATE_START,
    LEXER_STATE_COMMENT,
    LEXER_STATE_CHAR,
    LEXER_STATE_CHAR_FINISH,
    LEXER_STATE_CHAR_ESCAPE,
    LEXER_STATE_STRING,
    LEXER_STATE_STRING_ESCAPE,
    LEXER_STATE_DECIMAL_NUMBER,
    LEXER_STATE_DECIMAL_NUMBER_POST_SEPARATOR,
    LEXER_STATE_DECIMAL_OR_BINARY_OR_HEX_NUMBER,
    LEXER_STATE_BINARY_NUMBER,
    LEXER_STATE_BINARY_NUMBER_START,
    LEXER_STATE_BINARY_NUMBER_POST_SEPARATOR,
    LEXER_STATE_HEX_NUMBER,
    LEXER_STATE_HEX_NUMBER_START,
    LEXER_STATE_HEX_NUMBER_POST_SEPARATOR,
    LEXER_STATE_FP_NUMBER,
    LEXER_STATE_FP_NUMBER_START,
    LEXER_STATE_FP_NUMBER_POST_SEPARATOR,
    LEXER_STATE_KEYWORD_OR_IDENTIFIER,
    LEXER_STATE_MAYBE_EQUAL_EQUAL,
    LEXER_STATE_MAYBE_NOT_EQUAL,
    LEXER_STATE_MAYBE_GREATER_GREATER_OR_GREATER_EQUAL,
    LEXER_STATE_MAYBE_LESS_LESS_OR_LESS_EQUAL,
    LEXER_STATE_FINISHED,
    LEXER_STATE_FAILURE,
};

static const char *keyword_lexemes[] = {
    "var",      "const",  "function", "return", "for", "while", "break",
    "continue", "if",     "else",     "i8",     "i16", "i32",   "i64",
    "u8",       "u16",    "u32",      "u64",    "f32", "f64",   "bool",
    "char",     "string", "true",     "false",
};

static enum token keyword_tokens[] = {
    TOKEN_VAR,         TOKEN_CONST, TOKEN_FUNCTION, TOKEN_RETURN,
    TOKEN_FOR,         TOKEN_WHILE, TOKEN_BREAK,    TOKEN_CONTINUE,
    TOKEN_IF,          TOKEN_ELSE,  TOKEN_I8,       TOKEN_I16,
    TOKEN_I32,         TOKEN_I64,   TOKEN_U8,       TOKEN_U16,
    TOKEN_U32,         TOKEN_U64,   TOKEN_F32,      TOKEN_F64,
    TOKEN_BOOL,        TOKEN_CHAR,  TOKEN_STRING,   TOKEN_CONST_TRUE,
    TOKEN_CONST_FALSE,
};

static_assert(ARRLEN(keyword_tokens) == ARRLEN(keyword_lexemes));

i32 lexer_init(const char *string, struct lexer *lexer)
{
    lexer->string = string;
    lexer->last_c = string[0];

    lexer->cursor = 0;
    lexer->line = 1;

    return 0;
}

static bool is_ascii(char c)
{
    return isascii(c);
}

static bool is_newline(char c)
{
    return c == '\n' || c == '\r';
}

static bool is_space(char c)
{
    return c == ' ' || c == '\t' || c == '\v';
}

static bool is_letter(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

static bool is_digit(char c)
{
    return c >= '0' && c <= '9';
}

static bool is_binary_digit(char c)
{
    return c == '0' || c == '1';
}

static bool is_hex_digit(char c)
{
    return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') ||
           (c >= 'A' && c <= 'F');
}

static i32 push_to_lexeme(struct lexeme *lex, char c)
{
    if (lex->size >= LEXER_MAX_LEXEME_SIZE)
        return -1;

    lex->data[lex->size++] = c;
    lex->data[lex->size] = '\0';

    return 0;
}

static enum lexer_state run_start_state(char c, struct lexical_entry *entry)
{
    if (is_space(c) || is_newline(c)) {
        return LEXER_STATE_START;
    } else if (is_letter(c) || c == '_') {
        // TODO(José): Store failure information somewhere.
        if (push_to_lexeme(&entry->lexeme, c) != 0)
            return LEXER_STATE_FAILURE;
        return LEXER_STATE_KEYWORD_OR_IDENTIFIER;
    } else if (is_digit(c)) {
        // TODO(José): Store failure information somewhere.
        if (push_to_lexeme(&entry->lexeme, c) != 0)
            return LEXER_STATE_FAILURE;

        if (c != '0')
            return LEXER_STATE_DECIMAL_NUMBER;

        return LEXER_STATE_DECIMAL_OR_BINARY_OR_HEX_NUMBER;
    }

    enum lexer_state new_state = LEXER_STATE_FINISHED;
    switch (c) {
    case COMMENT_STARTER:
        new_state = LEXER_STATE_COMMENT;
        break;
    case '\'':
        // TODO(José): Store failure information somewhere.
        if (push_to_lexeme(&entry->lexeme, c) == 0)
            new_state = LEXER_STATE_CHAR;
        else
            new_state = LEXER_STATE_FAILURE;
        break;
    case '"':
        // TODO(José): Store failure information somehwere
        if (push_to_lexeme(&entry->lexeme, c) == 0)
            new_state = LEXER_STATE_STRING;
        else
            new_state = LEXER_STATE_FAILURE;
        break;
    case ':':
        entry->token = TOKEN_COLON;
        break;
    case ';':
        entry->token = TOKEN_SEMICOLON;
        break;
    case ',':
        entry->token = TOKEN_COMMA;
        break;
    case '+':
        entry->token = TOKEN_PLUS;
        break;
    case '-':
        entry->token = TOKEN_MINUS;
        break;
    case '/':
        entry->token = TOKEN_FORWARD_SLASH;
        break;
    case '*':
        entry->token = TOKEN_ASTERISK;
        break;
    case '%':
        entry->token = TOKEN_PERCENT;
        break;
    case '=':
        new_state = LEXER_STATE_MAYBE_EQUAL_EQUAL;
        break;
    case '!':
        new_state = LEXER_STATE_MAYBE_NOT_EQUAL;
        break;
    case '>':
        new_state = LEXER_STATE_MAYBE_GREATER_GREATER_OR_GREATER_EQUAL;
        break;
    case '<':
        new_state = LEXER_STATE_MAYBE_LESS_LESS_OR_LESS_EQUAL;
        break;
    case '&':
        entry->token = TOKEN_AMPERSAND;
        break;
    case '|':
        entry->token = TOKEN_PIPE;
        break;
    case '^':
        entry->token = TOKEN_CIRCUMFLEX;
        break;
    case '(':
        entry->token = TOKEN_LPAREN;
        break;
    case ')':
        entry->token = TOKEN_RPAREN;
        break;
    case '[':
        entry->token = TOKEN_LBRACKET;
        break;
    case ']':
        entry->token = TOKEN_RBRACKET;
        break;
    case '{':
        entry->token = TOKEN_LCURLY;
        break;
    case '}':
        entry->token = TOKEN_RCURLY;
        break;
    default:
        new_state = LEXER_STATE_FAILURE;
    }

    return new_state;
}

static enum lexer_state run_comment_state(char c)
{
    if (is_newline(c))
        return LEXER_STATE_START;
    return LEXER_STATE_COMMENT;
}

static enum lexer_state run_maybe_state(struct lexer *lexer, char c,
                                        char match_char, enum token match_token,
                                        enum token no_match_token,
                                        struct lexical_entry *entry)
{
    if (c == match_char) {
        entry->token = match_token;
    } else {
        entry->token = no_match_token;
        --lexer->cursor;
    }

    return LEXER_STATE_FINISHED;
}

enum lexer_state
run_maybe_greater_greater_or_greater_equal_state(struct lexer *lexer, char c,
                                                 struct lexical_entry *entry)
{
    if (c == '>') {
        entry->token = TOKEN_GREATER_GREATER;
    } else if (c == '=') {
        entry->token = TOKEN_GREATER_EQUAL;
    } else {
        entry->token = TOKEN_GREATER;
        --lexer->cursor;
    }

    return LEXER_STATE_FINISHED;
}

enum lexer_state
run_maybe_less_less_or_less_equal_state(struct lexer *lexer, char c,
                                        struct lexical_entry *entry)
{
    if (c == '<') {
        entry->token = TOKEN_LESS_LESS;
    } else if (c == '=') {
        entry->token = TOKEN_LESS_EQUAL;
    } else {
        entry->token = TOKEN_LESS;
        --lexer->cursor;
    }

    return LEXER_STATE_FINISHED;
}

enum lexer_state run_keyword_or_identifier_state(struct lexer *lexer, char c,
                                                 struct lexical_entry *entry)
{
    if (is_letter(c) || is_digit(c) || c == '_') {
        // TODO(José): Store failure information somewhere.
        if (push_to_lexeme(&entry->lexeme, c) != 0)
            return LEXER_STATE_FAILURE;

        return LEXER_STATE_KEYWORD_OR_IDENTIFIER;
    }

    --lexer->cursor;

    // NOTE(José): This linear search performing a strcmp is really not ideal,
    // but by doing it this way, we can check whether it's a keyword without
    // needing any extra information and if it's not then it must be an identifier.

    for (u32 i = 0; i < ARRLEN(keyword_lexemes); ++i) {
        const char *lexeme = keyword_lexemes[i];
        if (strcmp(lexeme, entry->lexeme.data) == 0) {
            entry->token = keyword_tokens[i];
            return LEXER_STATE_FINISHED;
        }
    }

    entry->token = TOKEN_IDENTIFIER;
    return LEXER_STATE_FINISHED;
}

enum lexer_state run_char_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (is_letter(c) || is_digit(c))
        return LEXER_STATE_CHAR_FINISH;
    else if (c == '\\')
        return LEXER_STATE_CHAR_ESCAPE;

    return LEXER_STATE_FAILURE;
}

enum lexer_state run_char_finish_state(char c, struct lexical_entry *entry)
{
    if (c != '\'')
        return LEXER_STATE_FAILURE;

    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    entry->token = TOKEN_CONST_CHAR;
    return LEXER_STATE_FINISHED;
}

enum lexer_state run_char_escape_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_CHAR_FINISH;
}

enum lexer_state run_string_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (c == '"') {
        entry->token = TOKEN_CONST_STRING;
        return LEXER_STATE_FINISHED;
    } else if (c == '\\') {
        return LEXER_STATE_STRING_ESCAPE;
    }

    return LEXER_STATE_STRING;
}

enum lexer_state run_string_escape_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_STRING;
}

enum lexer_state run_decimal_number_state(struct lexer *lexer, char c,
                                          struct lexical_entry *entry)
{
    if (c != '\'' && c != '.' && !is_digit(c)) {
        --lexer->cursor;
        entry->token = TOKEN_CONST_INTEGER;
        return LEXER_STATE_FINISHED;
    }

    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (c == '.')
        return LEXER_STATE_FP_NUMBER_START;
    else if (c == '\'')
        return LEXER_STATE_DECIMAL_NUMBER_POST_SEPARATOR;

    return LEXER_STATE_DECIMAL_NUMBER;
}

enum lexer_state
run_decimal_number_post_separator_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (!is_digit(c))
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_DECIMAL_NUMBER;
}

enum lexer_state
run_decimal_or_binary_or_hex_number_state(struct lexer *lexer, char c,
                                          struct lexical_entry *entry)
{
    if (c != '\'' && c != 'b' && c != 'x' && c != '.' && !is_digit(c)) {
        --lexer->cursor;
        entry->token = TOKEN_CONST_INTEGER;
        return LEXER_STATE_FINISHED;
    }

    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (c == '\'')
        return LEXER_STATE_DECIMAL_NUMBER_POST_SEPARATOR;
    else if (c == 'b')
        return LEXER_STATE_BINARY_NUMBER_START;
    else if (c == 'x')
        return LEXER_STATE_HEX_NUMBER_START;
    else if (c == '.')
        return LEXER_STATE_FP_NUMBER_START;

    return LEXER_STATE_DECIMAL_NUMBER;
}

enum lexer_state run_binary_number_start_state(char c,
                                               struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (!is_binary_digit(c))
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_BINARY_NUMBER;
}

enum lexer_state run_binary_number_state(struct lexer *lexer, char c,
                                         struct lexical_entry *entry)
{
    if (c != '\'' && !is_binary_digit(c)) {
        --lexer->cursor;
        entry->token = TOKEN_CONST_INTEGER;
        return LEXER_STATE_FINISHED;
    }

    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (c == '\'') {
        return LEXER_STATE_BINARY_NUMBER_POST_SEPARATOR;
    }

    return LEXER_STATE_BINARY_NUMBER;
}

enum lexer_state
run_binary_number_post_separator_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (!is_binary_digit(c))
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_BINARY_NUMBER;
}

enum lexer_state run_hex_number_start_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (!is_hex_digit(c))
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_HEX_NUMBER;
}

enum lexer_state run_hex_number_state(struct lexer *lexer, char c,
                                      struct lexical_entry *entry)
{
    if (c != '\'' && !is_hex_digit(c)) {
        --lexer->cursor;
        entry->token = TOKEN_CONST_INTEGER;
        return LEXER_STATE_FINISHED;
    }

    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (c == '\'') {
        return LEXER_STATE_HEX_NUMBER_POST_SEPARATOR;
    }

    return LEXER_STATE_HEX_NUMBER;
}

enum lexer_state
run_hex_number_post_separator_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (!is_hex_digit(c))
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_HEX_NUMBER;
}

enum lexer_state run_fp_number_state(struct lexer *lexer, char c,
                                     struct lexical_entry *entry)
{
    if (c != '\'' && !is_digit(c)) {
        --lexer->cursor;
        entry->token = TOKEN_CONST_FP;
        return LEXER_STATE_FINISHED;
    }

    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (c == '\'')
        return LEXER_STATE_FP_NUMBER_POST_SEPARATOR;

    return LEXER_STATE_FP_NUMBER;
}

enum lexer_state run_fp_number_start_state(char c, struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (!is_digit(c))
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_FP_NUMBER;
}

enum lexer_state run_fp_number_post_separator_state(char c,
                                                    struct lexical_entry *entry)
{
    // TODO(José): Store failure information somewhere.
    if (push_to_lexeme(&entry->lexeme, c) != 0)
        return LEXER_STATE_FAILURE;

    if (!is_digit(c))
        return LEXER_STATE_FAILURE;

    return LEXER_STATE_FP_NUMBER;
}

enum lexer_result lexer_get_next_entry(struct lexer *lexer,
                                       struct lexical_entry *entry)
{
    if (lexer->last_c == '\0')
        return LEXER_RESULT_EOF;

    ZEROPTR(entry);

    enum lexer_state state = LEXER_STATE_START;
    while (state != LEXER_STATE_FINISHED) {
        // TODO(José): Store and return information about error.
        if (state == LEXER_STATE_FAILURE)
            return LEXER_RESULT_ERROR;

        const char c = lexer->string[lexer->cursor++];
        lexer->last_c = c;

        if (lexer->last_c == '\0')
            return LEXER_RESULT_EOF;

        // TODO(José): Store and return information about error.
        if (!is_ascii(c))
            return LEXER_RESULT_ERROR;

        if (is_newline(c))
            ++lexer->line;

        switch (state) {
        case LEXER_STATE_START:
            state = run_start_state(c, entry);
            break;
        case LEXER_STATE_COMMENT:
            state = run_comment_state(c);
            break;
        case LEXER_STATE_CHAR:
            state = run_char_state(c, entry);
            break;
        case LEXER_STATE_CHAR_FINISH:
            state = run_char_finish_state(c, entry);
            break;
        case LEXER_STATE_CHAR_ESCAPE:
            state = run_char_escape_state(c, entry);
            break;
        case LEXER_STATE_STRING:
            state = run_string_state(c, entry);
            break;
        case LEXER_STATE_STRING_ESCAPE:
            state = run_string_escape_state(c, entry);
            break;
        case LEXER_STATE_DECIMAL_NUMBER:
            state = run_decimal_number_state(lexer, c, entry);
            break;
        case LEXER_STATE_DECIMAL_NUMBER_POST_SEPARATOR:
            state = run_decimal_number_post_separator_state(c, entry);
            break;
        case LEXER_STATE_DECIMAL_OR_BINARY_OR_HEX_NUMBER:
            state = run_decimal_or_binary_or_hex_number_state(lexer, c, entry);
            break;
        case LEXER_STATE_BINARY_NUMBER:
            state = run_binary_number_state(lexer, c, entry);
            break;
        case LEXER_STATE_BINARY_NUMBER_START:
            state = run_binary_number_start_state(c, entry);
            break;
        case LEXER_STATE_BINARY_NUMBER_POST_SEPARATOR:
            state = run_binary_number_post_separator_state(c, entry);
            break;
        case LEXER_STATE_HEX_NUMBER:
            state = run_hex_number_state(lexer, c, entry);
            break;
        case LEXER_STATE_HEX_NUMBER_START:
            state = run_hex_number_start_state(c, entry);
            break;
        case LEXER_STATE_HEX_NUMBER_POST_SEPARATOR:
            state = run_hex_number_post_separator_state(c, entry);
            break;
        case LEXER_STATE_FP_NUMBER:
            state = run_fp_number_state(lexer, c, entry);
            break;
        case LEXER_STATE_FP_NUMBER_START:
            state = run_fp_number_start_state(c, entry);
            break;
        case LEXER_STATE_FP_NUMBER_POST_SEPARATOR:
            state = run_fp_number_post_separator_state(c, entry);
            break;
        case LEXER_STATE_KEYWORD_OR_IDENTIFIER:
            state = run_keyword_or_identifier_state(lexer, c, entry);
            break;
        case LEXER_STATE_MAYBE_EQUAL_EQUAL:
            state = run_maybe_state(lexer, c, '=', TOKEN_EQUAL_EQUAL,
                                    TOKEN_EQUAL, entry);
            break;
        case LEXER_STATE_MAYBE_NOT_EQUAL:
            state = run_maybe_state(lexer, c, '=', TOKEN_NOT_EQUAL, TOKEN_NOT,
                                    entry);
            break;
        case LEXER_STATE_MAYBE_GREATER_GREATER_OR_GREATER_EQUAL:
            state = run_maybe_greater_greater_or_greater_equal_state(lexer, c,
                                                                     entry);
            break;
        case LEXER_STATE_MAYBE_LESS_LESS_OR_LESS_EQUAL:
            state = run_maybe_less_less_or_less_equal_state(lexer, c, entry);
            break;
        case LEXER_STATE_FAILURE:
        case LEXER_STATE_FINISHED:
            UNREACHABLE();
        }
    }

    RASSERT(entry->token != TOKEN_UNKNOWN);

    entry->line = lexer->line;
    return LEXER_RESULT_SUCCESS;
}

static const char *get_token_str(enum token tok)
{
    switch (tok) {
    case TOKEN_UNKNOWN:
        return "TOKEN_UNKNOWN";
    case TOKEN_IDENTIFIER:
        return "TOKEN_IDENTIFIER";
    case TOKEN_VAR:
        return "TOKEN_VAR";
    case TOKEN_CONST:
        return "TOKEN_CONST";
    case TOKEN_FUNCTION:
        return "TOKEN_FUNCTION";
    case TOKEN_RETURN:
        return "TOKEN_RETURN";
    case TOKEN_FOR:
        return "TOKEN_FOR";
    case TOKEN_WHILE:
        return "TOKEN_WHILE";
    case TOKEN_BREAK:
        return "TOKEN_BREAK";
    case TOKEN_CONTINUE:
        return "TOKEN_CONTINUE";
    case TOKEN_IF:
        return "TOKEN_IF";
    case TOKEN_ELSE:
        return "TOKEN_ELSE";
    case TOKEN_I8:
        return "TOKEN_I8";
    case TOKEN_I16:
        return "TOKEN_I16";
    case TOKEN_I32:
        return "TOKEN_I32";
    case TOKEN_I64:
        return "TOKEN_I64";
    case TOKEN_U8:
        return "TOKEN_U8";
    case TOKEN_U16:
        return "TOKEN_U16";
    case TOKEN_U32:
        return "TOKEN_U32";
    case TOKEN_U64:
        return "TOKEN_U64";
    case TOKEN_F32:
        return "TOKEN_F32";
    case TOKEN_F64:
        return "TOKEN_F64";
    case TOKEN_BOOL:
        return "TOKEN_BOOL";
    case TOKEN_CHAR:
        return "TOKEN_CHAR";
    case TOKEN_STRING:
        return "TOKEN_STRING";
    case TOKEN_CONST_TRUE:
        return "TOKEN_CONST_TRUE";
    case TOKEN_CONST_FALSE:
        return "TOKEN_CONST_FALSE";
    case TOKEN_CONST_CHAR:
        return "TOKEN_CONST_CHAR";
    case TOKEN_CONST_STRING:
        return "TOKEN_CONST_STRING";
    case TOKEN_CONST_INTEGER:
        return "TOKEN_CONST_INTEGER";
    case TOKEN_CONST_FP:
        return "TOKEN_CONST_FP";
    case TOKEN_COLON:
        return "TOKEN_COLON";
    case TOKEN_SEMICOLON:
        return "TOKEN_SEMICOLON";
    case TOKEN_COMMA:
        return "TOKEN_COMMA";
    case TOKEN_EQUAL:
        return "TOKEN_EQUAL";
    case TOKEN_EQUAL_EQUAL:
        return "TOKEN_EQUAL_EQUAL";
    case TOKEN_NOT_EQUAL:
        return "TOKEN_NOT_EQUAL";
    case TOKEN_GREATER:
        return "TOKEN_GREATER";
    case TOKEN_GREATER_EQUAL:
        return "TOKEN_GREATER_EQUAL";
    case TOKEN_LESS:
        return "TOKEN_LESS";
    case TOKEN_LESS_EQUAL:
        return "TOKEN_LESS_EQUAL";
    case TOKEN_PLUS:
        return "TOKEN_PLUS";
    case TOKEN_MINUS:
        return "TOKEN_MINUS";
    case TOKEN_FORWARD_SLASH:
        return "TOKEN_FORWARD_SLASH";
    case TOKEN_ASTERISK:
        return "TOKEN_ASTERISK";
    case TOKEN_PERCENT:
        return "TOKEN_PERCENT";
    case TOKEN_NOT:
        return "TOKEN_NOT";
    case TOKEN_AMPERSAND:
        return "TOKEN_AMPERSAND";
    case TOKEN_PIPE:
        return "TOKEN_PIPE";
    case TOKEN_CIRCUMFLEX:
        return "TOKEN_CIRCUMFLEX";
    case TOKEN_GREATER_GREATER:
        return "TOKEN_GREATER_GREATER";
    case TOKEN_LESS_LESS:
        return "TOKEN_LESS_LESS";
    case TOKEN_LPAREN:
        return "TOKEN_LPAREN";
    case TOKEN_RPAREN:
        return "TOKEN_RPAREN";
    case TOKEN_LBRACKET:
        return "TOKEN_LBRACKET";
    case TOKEN_RBRACKET:
        return "TOKEN_RBRACKET";
    case TOKEN_LCURLY:
        return "TOKEN_LCURLY";
    case TOKEN_RCURLY:
        return "TOKEN_RCURLY";
    }

    UNREACHABLE();
}

void lexer_print_entry(FILE *file, struct lexical_entry *entry)
{
    fprintf(file, "entry @ %u token %s ", entry->line,
            get_token_str(entry->token));

    bool should_print_lexeme;
    switch (entry->token) {
    case TOKEN_IDENTIFIER:
    case TOKEN_CONST_CHAR:
    case TOKEN_CONST_STRING:
    case TOKEN_CONST_INTEGER:
    case TOKEN_CONST_FP:
        should_print_lexeme = true;
        break;
    default:
        should_print_lexeme = false;
        break;
    }

    if (should_print_lexeme)
        fprintf(file, "lexeme `%s`", entry->lexeme.data);

    fputc('\n', file);
}

#if defined(ENABLE_TESTS)

static i32 test_lexer_matches_tok(const char *string, enum token token)
{
    struct lexer lexer;
    struct lexical_entry entry;

    RASSERT(lexer_init(string, &lexer) >= 0);

    const enum lexer_result r = lexer_get_next_entry(&lexer, &entry);
    if (r != LEXER_RESULT_SUCCESS)
        return -1;

    if (entry.token != token)
        return -1;

    return 0;
}

static i32 test_lexer_matches_tok_and_lexeme(const char *string,
                                             enum token token,
                                             const char *lexeme)
{
    struct lexer lexer;
    struct lexical_entry entry;

    RASSERT(lexer_init(string, &lexer) >= 0);

    const enum lexer_result r = lexer_get_next_entry(&lexer, &entry);
    if (r != LEXER_RESULT_SUCCESS)
        return -1;

    if (strcmp(entry.lexeme.data, lexeme) != 0)
        return -1;

    if (entry.token != token)
        return -1;

    return 0;
}

static i32 test_lexer_matches_result(const char *string,
                                     enum lexer_result result)
{
    struct lexer lexer;
    struct lexical_entry entry;

    RASSERT(lexer_init(string, &lexer) >= 0);

    const enum lexer_result r = lexer_get_next_entry(&lexer, &entry);
    if (r != result)
        return -1;

    return 0;
}

static i32 test_lexer_colon(void)
{
    return test_lexer_matches_tok(":\n", TOKEN_COLON);
}

static i32 test_lexer_semicolon(void)
{
    return test_lexer_matches_tok(";\n", TOKEN_SEMICOLON);
}

static i32 test_lexer_comma(void)
{
    return test_lexer_matches_tok(",\n", TOKEN_COMMA);
}

static i32 test_lexer_plus(void)
{
    return test_lexer_matches_tok("+\n", TOKEN_PLUS);
}

static i32 test_lexer_minus(void)
{
    return test_lexer_matches_tok("-\n", TOKEN_MINUS);
}

static i32 test_lexer_forward_slash(void)
{
    return test_lexer_matches_tok("/\n", TOKEN_FORWARD_SLASH);
}

static i32 test_lexer_asterisk(void)
{
    return test_lexer_matches_tok("*\n", TOKEN_ASTERISK);
}

static i32 test_lexer_percent(void)
{
    return test_lexer_matches_tok("%\n", TOKEN_PERCENT);
}

static i32 test_lexer_equal(void)
{
    return test_lexer_matches_tok("=\n", TOKEN_EQUAL);
}

static i32 test_lexer_equal_equal(void)
{
    return test_lexer_matches_tok("==\n", TOKEN_EQUAL_EQUAL);
}

static i32 test_lexer_not(void)
{
    return test_lexer_matches_tok("!\n", TOKEN_NOT);
}

static i32 test_lexer_not_equal(void)
{
    return test_lexer_matches_tok("!=\n", TOKEN_NOT_EQUAL);
}

static i32 test_lexer_greater(void)
{
    return test_lexer_matches_tok(">\n", TOKEN_GREATER);
}

static i32 test_lexer_greater_greater(void)
{
    return test_lexer_matches_tok(">>\n", TOKEN_GREATER_GREATER);
}

static i32 test_lexer_greater_equal(void)
{
    return test_lexer_matches_tok(">=\n", TOKEN_GREATER_EQUAL);
}

static i32 test_lexer_less(void)
{
    return test_lexer_matches_tok("<\n", TOKEN_LESS);
}

static i32 test_lexer_less_less(void)
{
    return test_lexer_matches_tok("<<\n", TOKEN_LESS_LESS);
}

static i32 test_lexer_less_equal(void)
{
    return test_lexer_matches_tok("<=\n", TOKEN_LESS_EQUAL);
}

static i32 test_lexer_ampersand(void)
{
    return test_lexer_matches_tok("&\n", TOKEN_AMPERSAND);
}

static i32 test_lexer_pipe(void)
{
    return test_lexer_matches_tok("|\n", TOKEN_PIPE);
}

static i32 test_lexer_circumflex(void)
{
    return test_lexer_matches_tok("^\n", TOKEN_CIRCUMFLEX);
}

static i32 test_lexer_lparen(void)
{
    return test_lexer_matches_tok("(\n", TOKEN_LPAREN);
}

static i32 test_lexer_rparen(void)
{
    return test_lexer_matches_tok(")\n", TOKEN_RPAREN);
}

static i32 test_lexer_lbracket(void)
{
    return test_lexer_matches_tok("[\n", TOKEN_LBRACKET);
}

static i32 test_lexer_rbracket(void)
{
    return test_lexer_matches_tok("]\n", TOKEN_RBRACKET);
}

static i32 test_lexer_lcurly(void)
{
    return test_lexer_matches_tok("{\n", TOKEN_LCURLY);
}

static i32 test_lexer_rcurly(void)
{
    return test_lexer_matches_tok("}\n", TOKEN_RCURLY);
}

static i32 test_lexer_eof(void)
{
    return test_lexer_matches_result("", LEXER_RESULT_EOF);
}

static i32 test_lexer_comment_start(void)
{
    return test_lexer_matches_result("#   +-=\n", LEXER_RESULT_EOF);
}

static i32 test_lexer_comment_end(void)
{
    return test_lexer_matches_tok("#   +-=\n+\n", TOKEN_PLUS);
}

static i32 test_lexer_line_count(void)
{
    struct lexer lexer;
    struct lexical_entry entry;

    RASSERT(lexer_init("\n+\n", &lexer) >= 0);

    const enum lexer_result r = lexer_get_next_entry(&lexer, &entry);
    if (r != LEXER_RESULT_SUCCESS)
        return -1;

    if (entry.line != 2)
        return -1;

    return 0;
}

static i32 test_lexer_non_ascii_failure(void)
{
    return test_lexer_matches_result("á\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_keyword(void)
{
    return test_lexer_matches_tok_and_lexeme("var\n", TOKEN_VAR, "var");
}

static i32 test_lexer_identifier(void)
{
    return test_lexer_matches_tok_and_lexeme("i\n", TOKEN_IDENTIFIER, "i");
}

static i32 test_lexer_char(void)
{
    return test_lexer_matches_tok_and_lexeme("'a'\n", TOKEN_CONST_CHAR, "'a'");
}

static i32 test_lexer_char_escape(void)
{
    return test_lexer_matches_tok_and_lexeme("'\\n'\n", TOKEN_CONST_CHAR,
                                             "'\\n'");
}

static i32 test_lexer_char_empty_error(void)
{
    return test_lexer_matches_result("''\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_char_incomplete_0_error(void)
{
    return test_lexer_matches_result("'\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_char_incomplete_1_error(void)
{
    return test_lexer_matches_result("'1\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_char_escape_incomplete_0_error(void)
{
    return test_lexer_matches_result("'\\     ", LEXER_RESULT_ERROR);
}

static i32 test_lexer_char_escape_incomplete_1_error(void)
{
    return test_lexer_matches_result("'\\n\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_string(void)
{
    return test_lexer_matches_tok_and_lexeme(
        "\"Hello, World!\"\n", TOKEN_CONST_STRING, "\"Hello, World!\"");
}

static i32 test_lexer_string_escape(void)
{
    return test_lexer_matches_tok_and_lexeme(
        "\"Hello,\\\" World!\"\n", TOKEN_CONST_STRING, "\"Hello,\\\" World!\"");
}

static i32 test_lexer_string_incomplete_error(void)
{
    return test_lexer_matches_result("\"Hello, World!\n", LEXER_RESULT_EOF);
}

static i32 test_lexer_string_escape_incomplete_error(void)
{
    return test_lexer_matches_result("\"Hello, World!\\\n", LEXER_RESULT_EOF);
}

static i32 test_lexer_string_empty(void)
{
    return test_lexer_matches_tok_and_lexeme("\"\"\n", TOKEN_CONST_STRING,
                                             "\"\"");
}

static i32 test_lexer_string_with_newline(void)
{
    return test_lexer_matches_tok_and_lexeme(
        "\"aaaaaa\nbbbbbb\"\n", TOKEN_CONST_STRING, "\"aaaaaa\nbbbbbb\"");
}

static i32 test_lexer_const_decimal_integer(void)
{
    return test_lexer_matches_tok_and_lexeme("123456\n", TOKEN_CONST_INTEGER,
                                             "123456");
}

static i32 test_lexer_const_decimal_integer_with_separators(void)
{
    return test_lexer_matches_tok_and_lexeme("0'234'567\n", TOKEN_CONST_INTEGER,
                                             "0'234'567");
}

static i32 test_lexer_const_decimal_integer_end_separator_error(void)
{
    return test_lexer_matches_result("1'234'\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_const_binary_integer(void)
{
    return test_lexer_matches_tok_and_lexeme("0b1\n", TOKEN_CONST_INTEGER,
                                             "0b1");
}

static i32 test_lexer_const_binary_integer_separator(void)
{
    return test_lexer_matches_tok_and_lexeme("0b1'1\n", TOKEN_CONST_INTEGER,
                                             "0b1'1");
}

static i32 test_lexer_const_binary_integer_incomplete_error(void)
{
    return test_lexer_matches_result("0b\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_const_binary_integer_separator_incomplete_error(void)
{
    return test_lexer_matches_result("0b1'\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_const_hex_integer(void)
{
    return test_lexer_matches_tok_and_lexeme("0x1a\n", TOKEN_CONST_INTEGER,
                                             "0x1a");
}

static i32 test_lexer_const_hex_integer_separator(void)
{
    return test_lexer_matches_tok_and_lexeme("0x1'a\n", TOKEN_CONST_INTEGER,
                                             "0x1'a");
}

static i32 test_lexer_const_hex_integer_incomplete_error(void)
{
    return test_lexer_matches_result("0x\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_const_hex_integer_separator_incomplete_error(void)
{
    return test_lexer_matches_result("0x1'\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_const_fp_0(void)
{
    return test_lexer_matches_tok_and_lexeme("0.123\n", TOKEN_CONST_FP,
                                             "0.123");
}

static i32 test_lexer_const_fp_1(void)
{
    return test_lexer_matches_tok_and_lexeme("1.123\n", TOKEN_CONST_FP,
                                             "1.123");
}

static i32 test_lexer_const_fp_separator_0(void)
{
    return test_lexer_matches_tok_and_lexeme("1'000.123\n", TOKEN_CONST_FP,
                                             "1'000.123");
}

static i32 test_lexer_const_fp_separator_1(void)
{
    return test_lexer_matches_tok_and_lexeme("1000.123'123\n", TOKEN_CONST_FP,
                                             "1000.123'123");
}

static i32 test_lexer_const_fp_separator_2(void)
{
    return test_lexer_matches_tok_and_lexeme("1'000.123'123\n", TOKEN_CONST_FP,
                                             "1'000.123'123");
}

static i32 test_lexer_const_fp_incomplete_error(void)
{
    return test_lexer_matches_result("1.\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_const_fp_separator_incomplete_error(void)
{
    return test_lexer_matches_result("1.1'\n", LEXER_RESULT_ERROR);
}

static i32 test_lexer_const_fp_separator_error(void)
{
    return test_lexer_matches_result("1.'1\n", LEXER_RESULT_ERROR);
}

static const struct test_case tests[] = {
    TEST(test_lexer_colon),
    TEST(test_lexer_semicolon),
    TEST(test_lexer_comma),
    TEST(test_lexer_plus),
    TEST(test_lexer_minus),
    TEST(test_lexer_forward_slash),
    TEST(test_lexer_asterisk),
    TEST(test_lexer_percent),
    TEST(test_lexer_equal),
    TEST(test_lexer_equal_equal),
    TEST(test_lexer_not),
    TEST(test_lexer_not_equal),
    TEST(test_lexer_greater),
    TEST(test_lexer_greater_greater),
    TEST(test_lexer_greater_equal),
    TEST(test_lexer_less),
    TEST(test_lexer_less_less),
    TEST(test_lexer_less_equal),
    TEST(test_lexer_ampersand),
    TEST(test_lexer_pipe),
    TEST(test_lexer_circumflex),
    TEST(test_lexer_lparen),
    TEST(test_lexer_rparen),
    TEST(test_lexer_lbracket),
    TEST(test_lexer_rbracket),
    TEST(test_lexer_lcurly),
    TEST(test_lexer_rcurly),
    TEST(test_lexer_eof),
    TEST(test_lexer_comment_start),
    TEST(test_lexer_comment_end),
    TEST(test_lexer_line_count),
    TEST(test_lexer_non_ascii_failure),
    TEST(test_lexer_keyword),
    TEST(test_lexer_identifier),
    TEST(test_lexer_char),
    TEST(test_lexer_char_escape),
    TEST(test_lexer_char_empty_error),
    TEST(test_lexer_char_incomplete_0_error),
    TEST(test_lexer_char_incomplete_1_error),
    TEST(test_lexer_char_escape_incomplete_0_error),
    TEST(test_lexer_char_escape_incomplete_1_error),
    TEST(test_lexer_string),
    TEST(test_lexer_string_escape),
    TEST(test_lexer_string_incomplete_error),
    TEST(test_lexer_string_escape_incomplete_error),
    TEST(test_lexer_string_empty),
    TEST(test_lexer_string_with_newline),
    TEST(test_lexer_const_decimal_integer),
    TEST(test_lexer_const_decimal_integer_with_separators),
    TEST(test_lexer_const_decimal_integer_end_separator_error),
    TEST(test_lexer_const_binary_integer),
    TEST(test_lexer_const_binary_integer_separator),
    TEST(test_lexer_const_binary_integer_incomplete_error),
    TEST(test_lexer_const_binary_integer_separator_incomplete_error),
    TEST(test_lexer_const_hex_integer),
    TEST(test_lexer_const_hex_integer_separator),
    TEST(test_lexer_const_hex_integer_incomplete_error),
    TEST(test_lexer_const_hex_integer_separator_incomplete_error),
    TEST(test_lexer_const_fp_0),
    TEST(test_lexer_const_fp_1),
    TEST(test_lexer_const_fp_separator_0),
    TEST(test_lexer_const_fp_separator_1),
    TEST(test_lexer_const_fp_separator_2),
    TEST(test_lexer_const_fp_incomplete_error),
    TEST(test_lexer_const_fp_separator_incomplete_error),
    TEST(test_lexer_const_fp_separator_error),
};

const struct test_case *get_lexer_tests(void)
{
    return tests;
}

i32 get_lexer_tests_len(void)
{
    return ARRLEN(tests);
}

#endif
