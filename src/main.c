#include <stdio.h>

#include "file_buffer.h"
#include "lexer.h"

i32 main(i32 argc, const char **argv)
{
    if (argc < 2) {
        return -1;
    }

    struct file_buffer file;
    if (file_buffer_read(argv[1], &file) < 0) {
        return -1;
    }

    struct lexer lexer;
    if (lexer_init(file.data, &lexer) < 0) {
        file_buffer_destroy(&file);
        return -1;
    }

    struct lexical_entry entry;
    enum lexer_result result = lexer_get_next_entry(&lexer, &entry);

    while (result == LEXER_RESULT_SUCCESS) {
        lexer_print_entry(stdout, &entry);
        result = lexer_get_next_entry(&lexer, &entry);
    }

    file_buffer_destroy(&file);
    return 0;
}
