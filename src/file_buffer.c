#include "file_buffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "utils.h"
#include "trace.h"

i32 file_buffer_read(const char *pathname, struct file_buffer *file)
{
    RASSERT(pathname);
    RASSERT(file);

    FILE *f = fopen(pathname, "rb");
    if (!f) {
        LOG_PERROR("fopen");
        return -1;
    }

    struct stat sb;
    if (fstat(fileno(f), &sb) < 0) {
        LOG_PERROR("fstat");
        goto err_out;
    }

    if (sb.st_size <= 0) {
        goto err_out;
    }

    const u64 size = (u64)sb.st_size;
    char *data = malloc(size);

    if (!data) {
        goto err_out;
    }

    if (fread(data, size, 1, f) != 1) {
        goto err_free;
    }

    fclose(f);

    file->data = data;
    file->size = size;
    return 0;

err_free:
    free(data);
err_out:
    fclose(f);
    return -1;
}

void file_buffer_destroy(struct file_buffer *file)
{
    free((void *)file->data);
    file->size = 0;
}
