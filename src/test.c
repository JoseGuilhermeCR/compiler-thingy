#include "test.h"

#include <stdio.h>

#include "lexer.h"
#include "trace.h"

#define SUCCEEDED_STR "\033[38;5;2msucceeded\033[0m"
#define FAILED_STR "\033[38;5;1mfailed\033[0m"

#define YELLOW_COLOR_STR "\033[38;5;3m"
#define RESET_COLOR_STR "\033[0m"

void run_tests(const char *name, i32 len, const struct test_case tests[len])
{
    LOG_INFO("Running tests for: %s\n", name);

    i32 succeeded = 0;
    i32 failed = 0;

    test_fn_t last_fn = NULL;

    for (i32 i = 0; i < len; ++i) {
        const struct test_case *t = &tests[i];

        if (last_fn == t->test)
            LOG_INFO(
                YELLOW_COLOR_STR
                "\tAbout to execute the same test twice in a row, maybe this is a mistake :^D\n" RESET_COLOR_STR);

        last_fn = t->test;

        const i32 ret = last_fn();
        if (ret == 0)
            ++succeeded;
        else
            ++failed;

        LOG_INFO("\t%s %s\n", t->name, ret == 0 ? SUCCEEDED_STR : FAILED_STR);
    }

    LOG_INFO("%i succeeded %i failed\n", succeeded, failed);
}

i32 main(void)
{
    run_tests("lexer", get_lexer_tests_len(), get_lexer_tests());
    return 0;
}
