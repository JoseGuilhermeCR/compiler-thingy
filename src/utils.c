#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

void assert_failed(const char *file, i32 line, const char *expr)
{
    fprintf(stderr, "(FATAL) [FAILED_ASSERTION] %s:%i => %s\n", file, line,
            expr);
    abort();
}

void unreachable_reached(const char *file, i32 line)
{
    fprintf(stderr, "(FATAL) [UNREACHABLE] %s:%i was reached\n", file, line);
    abort();
}
